'use strict';
const mongoose = require('mongoose');
const {DB_URI} = process.env;

mongoose.connect(DB_URI).catch(error => {
    console.log(error);
});


module.exports = {
    mongoose
};
