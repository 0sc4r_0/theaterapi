const {mongoose} = require('../db/db');


const roomSchema = mongoose.Schema({
    'name' : String,
    'price' : Number,
    'seats' : Number
});


const Room = mongoose.model('Room', roomSchema);

module.exports ={
    Room
};


