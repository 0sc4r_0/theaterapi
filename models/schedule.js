const { mongoose } = require('../db/db');



const scheduleSchema = mongoose.Schema({
    'startTime': Date,
    'endTime': Date,
    'movie': [{ type: mongoose.Schema.Types.ObjectId, ref: 'Movie' }],
    'room': [{ type: mongoose.Schema.Types.ObjectId, ref: 'Room' }]
});

const Schedule = mongoose.model('Schdeule', scheduleSchema);

module.exports = {
    Schedule
};