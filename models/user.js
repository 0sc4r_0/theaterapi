const {mongoose} = require('../db/db');


const userSchema = mongoose.Schema({
    'userName' : {type: String,require : true},
    'password' : {type: String,require : true},
    'rol'       :{type : String,requires : true}
});



const User = mongoose.model('User',userSchema);


module.exports = {
    User
};
