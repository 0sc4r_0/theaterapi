const {mongoose} = require('../db/db');



let movieSchema = mongoose.Schema({
    'imdbId' : String,
    'title' : String,
    'year'  : String,
    'rated' : String,
    'runtime' : String,
    'genre' : String,
    'plot' : String,
    'language' : String
    
});


let Movie = mongoose.model('Movie',movieSchema);

module.exports = {
    Movie
};
