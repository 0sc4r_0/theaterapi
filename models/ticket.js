const { mongoose } = require('../db/db');



const ticketSchema = mongoose.Schema({
    'userId': String,
    'movieId': [{ type: mongoose.Schema.Types.ObjectId, ref: 'Movie' }],
    'seats': Number,

});

const Ticket = mongoose.model('Ticket', ticketSchema);


module.exports = Ticket;