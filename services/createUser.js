'use strict';
const {User} = require('../models/user');


module.exports = (userName,password,rol) => {
    const userData = new User({userName,password,rol});
    return userData.save();

};