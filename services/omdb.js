const request = require('request-promise-native');
const {APIKEY} = process.env;
const {URI} = process.env;


module.exports = (parameters,type) =>{
         let parameter='';
    if(type === 'i'){
         parameter = parameters.id;
    }else{
        parameter = parameters.s;
    }
    let page = Number(parameters.page) || 1;
    return request(`${URI}?${type}=${parameter}&page=${page}&apikey=${APIKEY}`);
}
