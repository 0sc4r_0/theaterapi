'use strict';
const moment = require('moment');
const { Schedule } = require('../models/schedule');
const addEventTime = (startTime, runtime) => {
    const eventStart = moment(startTime, 'DD-MM-YYYY hh:mm').toDate();
    const eventTime = Number(runtime.slice(0, 3));
    const eventEnd = moment(eventStart).add(eventTime, 'minutes');
    const obj = [moment(eventStart).utc().toISOString(), moment(eventEnd).utc().toISOString()];
    return obj;
};



const checkDate = (start, end) => {
    end = moment(end).add(30,'minutes');
    return Schedule
        .findOne({
            $or: [
                { $and: [{ startTime: { $lte: start }, endTime: { $gte: start } }] },
                { $and: [{ startTime: { $lt: end }, endTime: { $gte: start } }] },
                { $and: [{ startTime: { $gte: start }, endTime: { $lt: end } }] }
            ]
        })
        . exec()
        .then(result => {
            if (result) return true;
            return false;
        });
};




module.exports = {
    addEventTime,
    checkDate

};

