'use strict';
const {Schedule} = require('../models/schedule');


module.exports = (req,res) => {
    const limit = Number(req.query.limit) || 5;
    const page =  Number(req.query.page) || 1;
    Schedule.find().select('-room -_id -__v -endTime').limit(limit).skip(page)
    .populate({path:'movie',model : 'Movie',select: 'title rated runtime genre plot language -_id'})
    .exec((err,schedule)=>{
        if(err) return res.status(500).send({error: err.message});
        res.status(200).send({schedule});
    });
};