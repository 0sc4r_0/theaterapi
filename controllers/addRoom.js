'use strict';

const {Room} = require('../models/room');

module.exports = (req,res) => {
    let {name,price,seats} = req.body;
    if(Object.keys(req.body).length === 0 || name === ''|| price === ''|| price <= 0 || seats <=0) 
    return res.status(422).send('wrong body parameters');
    Room.findOne({name})
    .then(exists =>{
        if(exists) return res.status(409).send('room already exists');
        const Data = new Room({name,price,seats});
        return Data.save()
    .then(room => {
            res.status(200).send(room);
    });
    })
   .catch(error =>{
       res.status(500).send(error.message);
   });
};