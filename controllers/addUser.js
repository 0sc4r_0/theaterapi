'use strict';
const { User } = require('../models/user');
const bcrypt = require('bcryptjs');
const createUser = require('../services/createUser');

module.exports = (req, res) => {
    const { userName, password, rol } = req.body;
    if (Object.keys(req.body).length === 0 || !userName || !password || !rol)
        return res.status(422).send('wrong body parameters');
    const hashedPass = bcrypt.hashSync(password, 10);
    return User.findOne({ userName })
        .then(exists => {
            if (exists) return res.status(409).send('username already exists');
            if (req.userRol === 'admin')
                return createUser(userName, hashedPass, rol)
                    .then(user => {
                        res.status(200).send(user);
                    });
            if (rol) return res.status(403).send('you dont have authorization to  set roles');
            return createUser(userName, hashedPass, 'client')
                .then(user => {
                    res.status(200).send(user);
                });
        })
        .catch(error => {
            res.status(500).send(error.message);
        });
};