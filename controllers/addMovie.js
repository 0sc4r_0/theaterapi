'use strict';
const omdb = require('../services/omdb');
const {Movie} = require('../models/movie');
module.exports = (req,res) => {
    if(Object.keys(req.body).length === 0) return res.status(422).send('wrong body parameters');
    omdb(req.body,'i')
    .then(movie => {
        const movieData = JSON.parse(movie);
        if(movieData.Response !== 'True') return res.status(404).send('movie not found');
        Movie.findOne({imdbId : req.body.id})
    .then(result => {
        if(result) return res.status(409).send('movie exists');
        const Data = new Movie({
            imdbId : movieData.imdbID,
            title : movieData.Title,
            year  : movieData.Year,
            rated : movieData.Rated,
            runtime : movieData.Runtime,
            genre : movieData.Genre,
            plot :  movieData.Plot,
            language : movieData.Language
        });
        return Data.save()
    .then(movie => {
        res.status(200).send(movie);
         });
    });       
    })
    .catch(error =>{
        res.status(500).send(error.message);
    });
};