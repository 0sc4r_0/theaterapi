'use strict';

const getMovie = require('./getMovie');
const addMovie = require('./addMovie');
const addUser = require('./addUser');
const addRoom = require('./addRoom');
const login = require('./login');
const addSchedule = require('./addSchedule');
const logOut = require('./logOut');
const getSchedule = require('./getSchedules');
module.exports = {
    getMovie,
    addMovie,
    addUser,
    addRoom,
    login,
    addSchedule,
    logOut,
    getSchedule
};