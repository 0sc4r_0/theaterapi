'use strict';
const jwt = require('jsonwebtoken');
const { User } = require('../models/user');
const bcrypt = require('bcryptjs');
const { SECRET } = process.env;

module.exports = (req, res) => {
    const { username, password } = req.body;
    if (Object.keys(req.body).length === 0 || !username || typeof (password) === null)
        return res.status(200).send('wrong body parameters');
    User.findOne({ userName: username })
        .then(user => {
            if (!user)
                return res.status(404).send('wrong username or password');
            const validPassword = bcrypt.compareSync(password, user.password);
            if (!validPassword) return res.status(404).send('wrong username or password');
            const payLoad = { id: user.id, rol: user.rol };
            const token = jwt.sign(payLoad, SECRET, { expiresIn: 86400 });
            res.status(200).json({ token });
        }).catch(error => {
            res.status(500).json(error.message);
        });
};


