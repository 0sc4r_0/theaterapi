'use strict';
const { Room } = require('../models/room');
const { Movie } = require('../models/movie');
const { Schedule } = require('../models/schedule');
const moment = require('moment');
const dates = require('../services/validateTime');

module.exports = (req, res) => {
    let { startTime, movieId, roomId } = req.body;
    const validDate = moment(startTime, 'DD-MM-YYYY hh:mm').isValid();
    if (!validDate)
        return res.status(422).json({ 'message': 'wrong format date' });
    if (!(movieId) || movieId === '' || !(roomId) || roomId === '')
        return res.status(422).json({ 'message': 'wrong parameters' });
    return Promise.all([Room.findById({ _id: roomId }), Movie.findById({ _id: movieId })])
        .then(([room, movie]) => {
            if (!room || !movie) return Promise.reject({ StatusCode: 404, message: 'movie or room not found' });
            const [eventStart, eventEnd] = dates.addEventTime(startTime, movie.runtime);
            return Promise.all([dates.checkDate(eventStart, eventEnd), { eventStart, eventEnd, movieId, roomId }]);
        })
        .then(([exists, movie]) => {
            if (exists)
                return Promise.reject({ statusCode: 409, message: 'there is a movie booked already' });
            const scheduleData = new Schedule({
                'startTime': movie.eventStart,
                'endTime': movie.eventEnd,
                'movie': movie.movieId,
                'room': movie.roomId
            });
            return scheduleData.save();
        })
        .then(schedule => {
            if (schedule) return res.status(200).json({ 'result': schedule });
            return Promise.
                reject({ statusCode: 500, message: 'there has been an error with the database' });
        })
        .catch(error => {
            res.status(error.statusCode).json({ 'error': error.message });
        });
};
