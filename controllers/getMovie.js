'use strict';

const omdb = require('../services/omdb');

module.exports = (req,res) => {
        omdb(req.query,'s')
        .then(movie => {
                const movieData = JSON.parse(movie);
                if(movieData.Response !== 'True') return res.status(404).send('movie not found');
                res.status(200).send(movieData.Search);
        }).catch(error =>{
                res.status(500).send({error});
        });
};