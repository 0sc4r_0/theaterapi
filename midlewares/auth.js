'use strict';
const jwt = require('jsonwebtoken');
const {SECRET} = process.env;

const tokenVerify = (req,res,next) => {
    const token = req.headers['auth-token'];
    if(!token) return res.status(401).send({message : 'token has not been set'});
    jwt.verify(token,SECRET , (err, decoded) => {
        if (err) return res.status(500).send('Failed to authenticate token.');
        req.userId = decoded.id;
        req.userRol = decoded.rol;
        next();
      });
};
const verifyCreation = (req,res,next) => {
    const token = req.headers['auth-token'];
    if(!token) return next();
    jwt.verify(token,SECRET , (err, decoded) => {
        if (err) return res.status(500).send('Failed to authenticate token.');
        req.userId = decoded.id;
        req.userRol = decoded.rol;
        next();
      });

    
};

const verifyAdmin = (req,res,next) =>{
    if(req.userRol !== 'admin') 
    return res.status(403).send({message:'you dont have admin privileges'});
    next();
};

const verifyCredential = (req,res,next) =>{
    if(req.userRol === 'employee' || req.userRol === 'admin' ) return next();
     res.status(403).send({message:'you dont have enough privileges'});
};
module.exports = {
    tokenVerify,
    verifyAdmin,
    verifyCreation,
    verifyCredential

};