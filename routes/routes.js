'use strict';
const controller = require('../controllers/index');
const auth = require('../midlewares/auth'); 
module.exports = (app) => {
    app.post('/users/login',controller.login);
    app.post('/users',auth.verifyCreation,controller.addUser);
    app.get('/omdbapi/movies',auth.tokenVerify,auth.verifyAdmin,controller.getMovie);
    app.post('/omdbapi/movies',auth.tokenVerify,auth.verifyAdmin,controller.addMovie);
    app.get('/schedules/movies',controller.getSchedule);
    app.get('/movie/id/schedules');
    app.post('/schedules',auth.tokenVerify,auth.verifyCredential,controller.addSchedule);
    app.post('/schedules/id/tickets');
    app.post('/rooms',auth.tokenVerify,auth.verifyAdmin,controller.addRoom);
    app.get('/tickets/sales',auth.tokenVerify,auth.verifyAdmin);
    app.delete('/logout',controller.logOut);
};
